## Prerrequisitos

Como requisito previo requiere la instalación de python 3. Para poder usar el Start es necesario disponer de Bash, es decir que lo idea es estar en linux. Si estas en linux dejo la documentación necesaria para poder ejecutar el start.

### Debian/Ubuntu

```bash
sudo apt install python3 git
```

### Arch + yay

```bash
sudo yay -S python3 git
```

### Arch

```bash
sudo pacman -S python3 git
```

### PIP install

Además de todo esto que instalamos, es necesario realizar la instalación de pycrypto para poder encriptar y desencriptar. Esto lo vamos a hacer con pip3 de la siguiente manera.

```bash
pip3 install pycryptodomex
```

---

## Uso

Una vez que tengamos todo instalado podemos descargarlo y ejecutarlo para lo cual voy a dejarte el comando para poder ejecutarlo con simpleza

```bash
git clone https://gitlab.com/JoaquinDecima/aesfileencrypt
cd aesfileencrypt
./aesencript.sh dir/file
```

Donde dir/file es el directorio del archivo que queremos encriptar.

---

## Información

* **Autor:** Joaquin (Pato) Decima
* **Año:** 2020
* **Instituto:** Universidad Nacional de Quilmes
* **Clase:** Seguridad Informática
