#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
import sys;
from encrypt.aes import AESCipher;

dirFile = sys.argv[1];
mainFile = open(dirFile,"r");
texto = mainFile.read();

while True:
    print("###############################################################################");
    print("##                                                                           ##");
    print("## Desea encriptar o desencriptar el archivo seleccionado, en caso de querer ##");
    print("##    encriptarlo por favor ingrese e, y en caso de querer desencriptarlo    ##");
    print("##                              ingrese d (e/d)                              ##");
    print("##                                                                           ##");
    print("###############################################################################");
    print("");
    print("");
    selector = input();

    if selector == "e":
        print("###############################################################################");
        print("##                                                                           ##");
        print("## Encriptamos su contenido, para esto requerimos de la contraseña, una que  ##");
        print("##   solo usted sabrá y le permitirá desencriptar el archivo más adelante.   ##");
        print("##                                                                           ##");
        print("###############################################################################");
        print("");
        print("");
        key = input()

        aes = AESCipher(key);
        encriptado= aes.encrypt(texto);

        textEncript = encriptado["cipher_text"];
        salt = encriptado["salt"];
        tag = encriptado["tag"];
        nonce = encriptado["nonce"];

        fencript = open("encriptado", "w+");
        fcontrol = open("control", "w+");

        fencript.write(textEncript);
        fcontrol.write("salt = " + salt + "\ntag = " + tag + "\nnonce = " + nonce);

        print("###############################################################################");
        print("##                                                                           ##");
        print("##    Ya encriptamos tu contenido, recuerda que la clave es algo que solo    ##");
        print("##       sabras vos, el contenido encriptado se encuentra en el archivo      ##");
        print("## encriptado. La información de desencriptado (sin contar la contraseña) se ##");
        print("##                      encuentra en el archivo control.                     ##");
        print("##                                                                           ##");
        print("###############################################################################");
        print("");
        print("");

        break

    if selector == "d":
        print("###############################################################################");
        print("##                                                                           ##");
        print("## Para desencriptar el contenido asegúrese de tener en archivo control que  ##");
        print("##  se creó al encriptarlo. Por favor facilite la contraseña de encriptado,  ##");
        print("##              la misma NO se encuentra en el archivo control.              ##")
        print("##                                                                           ##");
        print("###############################################################################");
        print("");
        print("");
        key = input();
        aes = AESCipher(key);

        print("###############################################################################");
        print("##                                                                           ##");
        print("##            Busque dentro el archivo control y brindanos el salt           ##");
        print("##                                                                           ##");
        print("###############################################################################");
        print("");
        print("");
        salt = input();

        print("###############################################################################");
        print("##                                                                           ##");
        print("##            Busque dentro el archivo control y brindanos el tag            ##");
        print("##                                                                           ##");
        print("###############################################################################");
        print("");
        print("");
        tag = input();

        print("###############################################################################");
        print("##                                                                           ##");
        print("##           Busque dentro el archivo control y brindanos el nonce           ##");
        print("##                                                                           ##");
        print("###############################################################################");
        print("");
        print("");
        nonce = input();

        fdesencript = open("desencriptado", "w+");
        fdesencript.write(aes.decrypt(salt, texto, nonce, tag).decode("utf-8") );

        print("###############################################################################");
        print("##                                                                           ##");
        print("##                Ya podes encontrar el archivo desencriptado.               ##");
        print("##                                                                           ##");
        print("###############################################################################");
        print("");
        print("");

        break
