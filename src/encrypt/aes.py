from base64 import b64encode, b64decode
import hashlib
from Cryptodome.Cipher import AES
import os
from Cryptodome.Random import get_random_bytes

class AESCipher(object):

   def __init__(self, key):
       self.bs = 32
       self.key = hashlib.sha256(key.encode()).digest()

   def encrypt(self, plain_text):
        # generate a random salt
        salt = get_random_bytes(AES.block_size)

        # use the Scrypt KDF to get a private key from the self.key
        private_key = hashlib.scrypt(
            self.key, salt=salt, n=2**14, r=8, p=1, dklen=32)

        # create cipher config
        cipher_config = AES.new(private_key, AES.MODE_GCM)

        # return a dictionary with the encrypted text
        cipher_text, tag = cipher_config.encrypt_and_digest(bytes(plain_text, 'utf-8'))
        return {
            'cipher_text': b64encode(cipher_text).decode('utf-8'),
            'salt': b64encode(salt).decode('utf-8'),
            'nonce': b64encode(cipher_config.nonce).decode('utf-8'),
            'tag': b64encode(tag).decode('utf-8')
        }


   def decrypt(self, psalt, ptext, pnonce, ptag):
      # decode the dictionary entries from base64
      salt = b64decode(psalt)
      cipher_text = b64decode(ptext)
      nonce = b64decode(pnonce)
      tag = b64decode(ptag)


      # generate the private key from the password and salt
      private_key = hashlib.scrypt(
         self.key, salt=salt, n=2**14, r=8, p=1, dklen=32)

      # create the cipher config
      cipher = AES.new(private_key, AES.MODE_GCM, nonce=nonce)

      # decrypt the cipher text
      decrypted = cipher.decrypt_and_verify(cipher_text, tag)

      return decrypted
